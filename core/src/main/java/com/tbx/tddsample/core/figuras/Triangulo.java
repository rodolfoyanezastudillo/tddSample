package com.tbx.tddsample.core.figuras;

public class Triangulo implements Figura {

	int base;
	int lateral;
	int lateralB;
	
	public Triangulo(int ladoEquilatero){
	
		this(ladoEquilatero, ladoEquilatero, ladoEquilatero);
	
	}
	public Triangulo(int base, int lado){
	
		this(base,lado,lado);
	
	}
	public Triangulo(int base, int lado, int otroLado){

		this.setBase(base);
		this.setLateral(lado);
		this.setLateralB(otroLado);
	
	}
	
	public int getPerimetro() {
		
		return base+lateral+lateralB;
	}

	public int getBase() {
		return base;
	}

	public void setBase(int base) {
		this.base = base;
	}

	public int getLateral() {
		return lateral;
	}

	public void setLateral(int lateral) {
		this.lateral = lateral;
	}

	public int getLateralB() {
		return lateralB;
	}

	public void setLateralB(int lateralB) {
		this.lateralB = lateralB;
	}

	
	
}
