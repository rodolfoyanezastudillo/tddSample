package com.tbx.tddsample.core.figuras;

public interface Figura {

	int getPerimetro();
	
}
