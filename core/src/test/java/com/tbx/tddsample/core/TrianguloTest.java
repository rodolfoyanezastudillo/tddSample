package com.tbx.tddsample.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.tbx.tddsample.core.figuras.Figura;
import com.tbx.tddsample.core.figuras.Triangulo;

public class TrianguloTest {

	@Before
	public void setUp() {
	  System.out.println("setup");
	}
	
	@Test
	public void getPerimetro_equilatero30_perimetro90() {
		Figura triangulo = new Triangulo(30);
		int perimetro = triangulo.getPerimetro();
		
		assertEquals(90, perimetro);
		
	}
	
	@Test
	public void getPerimetro_equilatero40_perimetro120() {
		Figura triangulo = new Triangulo(40);
		int perimetro = triangulo.getPerimetro();
		
		assertEquals(120, perimetro);
		
	}

}
