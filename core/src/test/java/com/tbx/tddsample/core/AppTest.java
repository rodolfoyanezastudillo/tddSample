package com.tbx.tddsample.core;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class AppTest 
{
	@Test
	public void stringCalc_emptyString_returnsZero(){
	
		App app = new App();
		int respuesta = app.stringCalc();
		assertEquals(0,respuesta);
	
	}
}
